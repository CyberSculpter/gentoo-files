#!/bin/bash
mkfs.ext4 -L "Gentoo" /dev/nvme0n1p2
mkfs.vfat -F32 /dev/nvme0n1p1
mkdir /mnt/gentoo
mount /dev/nvme0n1p2 /mnt/gentoo
mkdir -p /mnt/gentoo/boot/efi
mount /dev/nvme0n1p1 /mnt/gentoo/boot/efi
cd /mnt/gentoo
wget https://gentoo.osuosl.org/releases/amd64/autobuilds/current-stage3-amd64-openrc/stage3-amd64-openrc-20211128T170532Z.tar.xz
tar xpvf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner
wget https://gitlab.com/bfitzgit23/gentoo-files/-/raw/main/make.conf -O /mnt/gentoo/etc/portage/make.conf
chown -R root:root /mnt/gentoo/etc/portage/make.conf
mkdir --parents /mnt/gentoo/etc/portage/repos.conf
cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf
cp --dereference /etc/resolv.conf /mnt/gentoo/etc/
mount --types proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
chroot /mnt/gentoo /bin/bash
