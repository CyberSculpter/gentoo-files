#!/bin/bash
emerge-webrsync
emerge vim dev-vcs/git
emerge -avuND @world
echo "America/Kentucky/Louisville" > /etc/timezone
emerge --config sys-libs/timezone-data
echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8 UTF-8" >> /etc/env.d/02locale
echo "LC_COLLATE=C.UTF-8" >> /etc/env.d/02locale
env-update && source /etc/profile && export PS1="(chroot) ${PS1}"
echo "sys-kernel/gentoo-sources ~amd64" >> /etc/portage/package.accept_keywords/kernel
emerge --ask sys-kernel/linux-firmware gentoo-sources sys-kernel/genkernel
eselect kernel set 1
echo "/dev/nvme0n1p1 /boot/efi vfat defaults,noatime 0 0" >> /etc/fstab
echo "/dev/nvme0n1p2 /	       ext4 defaults,noatime 1 1" >> /etc/fstab
echo "/dev/sr0	     /mnt/cdrom auto    noauto,user  0 0" >> /etc/fstab
echo "/dev/sdb1	     /Backup ntfs    defaults  0 0" >> /etc/fstab
genkernel all
echo "MainPC-Gentoo" >> /etc/conf.d/hostname
emerge --noreplace net-misc/netifrc
echo "config_enp5s0=dhcp" >> /etc/conf.d/net
ln -s /etc/init.d/net.lo /etc/init.d/net.enp5s0
rc-update add net.enp5s0 default
rc-update add dhcpcd default
echo "127.0.0.1 MainPC-Gentoo localhost" >> /etc/hosts
echo ">=sys-boot/grub-2.06-r1 mount" >> /etc/portage/package.use/zz-autounmask
emerge cronie dhcpcd ntfs3g os-prober grub
rc-update add cronie default
rc-update add sshd default
grub-install --target=x86_64-efi --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg

echo "dev-lang/rust" >> /etc/portage/package.mask
echo "net-im/discord-bin ~amd64" >> /etc/portage/package.accept_keywords/discord
echo "www-client/firefox-bin ~amd64" >> /etc/portage/package.accept_keywords/firefox-bin
echo "net-misc/anydesk ~amd64" >> /etc/portage/package.accept_keywords/anydesk
echo "x11-libs/gtkglext ~amd64" >> /etc/portage/package.accept_keywords/anydesk
echo "app-portage/kuroo" >> /etc/portage/package.accept_keywords/kuroo
echo "kde-plasma/plasma-meta browser-integration discover -sddm legacy-systray networkmanager -sdk wallpapers" >> /etc/portage/package.use/plasma
echo "kde-plasma/discover flatpak" >> /etc/portage/package.use/discover
emerge --oneshot harfbuzz
emerge -av xorg-x11 xorg-drivers plasma plasma-meta lightdm lightdm-gtk-greeter firefox-bin mousepad eix discord-bin kuroo kdesu alsa-utils alsa-lib alsa-plugins pavucontrol
eix-update
rc-update add dbus default
echo "DISPLAYMANAGER=lightdm" > /etc/conf.d/display-manager
rc-update add display-manager default
rc-update add anydesk default
rc-update add NetworkManager default && rc-update del net.enp5s0 default
rc-update add bluetooth default
mkdir /Backup
